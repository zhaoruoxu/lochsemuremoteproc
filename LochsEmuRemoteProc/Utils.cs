﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LochsEmuRemoteProc.Native;

namespace LochsEmuRemoteProc
{
    public class Utils
    {
        public static string GetPathByHandle(IntPtr hFile)
        {
            var path = new StringBuilder((int)Win32Api.MAX_PATH);
            Win32Api.GetFinalPathNameByHandle(hFile, path,
                Win32Api.MAX_PATH, Win32Api.FILE_NAME_NORMALIZED);
            return path.ToString();
        }
    }
}
