﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using LochsEmuRemoteProc.Native;
using LochsEmuRemoteProc.Comm;

namespace LochsEmuRemoteProc
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Argument is remote IP; '.' represents localhost");
                return;
            }

            if (IntPtr.Size == sizeof(Int32))
            {
                Console.WriteLine("Remote debugger running on 32-bit platform");
            }
            else if (IntPtr.Size == sizeof (Int64))
            {
                Console.WriteLine("Unsupported platform: 64-bit");
                return;
            }

            var engine = new Engine();
            engine.Run(args[0]);
            return;

            //var proc = new NativeProcess();
            //proc.Load();

            //var pipe = new PipeClient();
            //pipe.Connect("192.168.0.104", "lochsemu");
            //return;

            const string DebugAppPath = @"e:\Code\LochsEmuSamples\TaintThread\Release\TaintThread.exe";

            //var engine = new Engine();
            var process = engine.CreateProcessDebug(DebugAppPath, null);
            while (true)
            {
                var nativeEvent = process.WaitForDebugEvent();
                Console.WriteLine(nativeEvent.ToString());
                if (nativeEvent is ExitProcessDebugEvent)
                {
                    break;
                }
                process.ContinueDebugEvent(nativeEvent);
            }
        }
    }
}
