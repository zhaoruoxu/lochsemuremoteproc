﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using LochsEmuRemoteProc.Comm;

namespace LochsEmuRemoteProc.Native
{
    class NativeModuleInfo
    {
        public readonly UInt32 _baseAddr;
        public readonly string _imagePath;

        public UInt32 BaseAddress { get { return _baseAddr; } }
        public string ImagePath { get { return _imagePath; } }

        public NativeModuleInfo(UInt32 baseAddr, string imagePath)
        {
            _baseAddr = baseAddr;
            _imagePath = imagePath;
        }
    }

    class NativeThreadInfo
    {
        private readonly uint _threadId;
        private readonly IntPtr _handle;
        private readonly UInt32 _baseAddr;
        private readonly UInt32 _remoteId;

        public uint ThreadId { get { return _threadId; } }
        public IntPtr Handle { get { return _handle; } }
        public UInt32 BaseAddr { get { return _baseAddr; } }
        public UInt32 RemoteId { get { return _remoteId; } }
        public UInt32 OffsetEip { get; set; }
        public UInt32 OffsetEsp { get; set; }

        public NativeThreadInfo(uint id, IntPtr handle, UInt32 baseAddr, UInt32 remoteId, UInt32 offEip, UInt32 offEsp)
        {
            _threadId = id;
            _handle = handle;
            _baseAddr = baseAddr;
            _remoteId = remoteId;
            OffsetEip = offEip;
            OffsetEsp = offEsp;
        }
    }

    public class NativeProcess
    {
        private readonly uint _pid;
        private readonly uint _tid;
        private UInt32 _offsetToEmu;
        private IntPtr _handle;
        private IntPtr _handleThread;
        private UInt32 _baseOfImage;
        private UInt32 _startAddress;
        private string _path;
        private readonly Dictionary<UInt32, NativeModuleInfo> _moduleInfos = new Dictionary<uint, NativeModuleInfo>();
        private readonly Dictionary<uint, NativeThreadInfo> _threadInfos = new Dictionary<uint, NativeThreadInfo>(); 
        private readonly Dictionary<uint, Queue<NativeEvent>> _eventQueue = new Dictionary<uint, Queue<NativeEvent>>();
        private readonly Dictionary<UInt32, uint> _tidMap = new Dictionary<uint, uint>();
        private readonly Dictionary<uint, bool> _eventContinued = new Dictionary<uint, bool>(); 

        public NativeProcess(uint pid, uint tid)
        {
            _pid = pid;
            _tid = tid;
        }

        internal uint ProcessId { get { return _pid; } }
        internal uint ThreadId { get { return _tid; } }
        internal IntPtr Handle { get { return _handle; } }
        internal IntPtr HandleThread { get { return _handleThread; } }
        internal UInt32 BaseOfImage { get { return _baseOfImage; } }
        internal UInt32 StartAddress { get { return _startAddress; } }
        internal string Path { get { return _path; } }
        internal UInt32 Offset { get { return _offsetToEmu; } }

        internal NativeThreadInfo GetThreadInfo(uint tid)
        {
            return _threadInfos[tid];
        }

        internal bool HasThread(uint tid)
        {
            return _threadInfos.ContainsKey(tid);
        }

        internal void Init(ref CREATE_PROCESS_DEBUG_INFO info)
        {
            _handle = info.hProcess;
            _baseOfImage = info.lpBaseOfImage;
            _startAddress = info.lpStartAddress;
            _path = Utils.GetPathByHandle(info.hFile).Substring(4);
            _handleThread = info.hThread;
            AddModule(_baseOfImage, _path);
        }

        internal void CalculateOffset(UInt32 imagebase, UInt32 startaddr)
        {
            UInt32 offsetImageBase = unchecked(BaseOfImage - imagebase);
            UInt32 offsetStartAddr = unchecked(StartAddress - startaddr);
            if (offsetImageBase != offsetStartAddr)
            {
                throw new Exception("Offset mismatch");
            }
            _offsetToEmu = offsetImageBase;

            //AddThread(ThreadId, HandleThread, StartAddress, _offsetToEmu);
        }

        internal void ClearHandle()
        {
            if (_handle == IntPtr.Zero)
            {
                throw new InvalidOperationException("Handle is IntPtr.Zero");
            }
            _handle = IntPtr.Zero;
        }

        internal void AddModule(UInt32 baseAddr, string imagePath)
        {
            if (_moduleInfos.ContainsKey(baseAddr))
            {
                throw new InvalidOperationException(
                    string.Format("Module [{0:x08}, {1}] exists", baseAddr, imagePath));
            }
            _moduleInfos.Add(baseAddr, new NativeModuleInfo(baseAddr, imagePath));
        }

        internal void AddThread(uint tid, IntPtr handle, UInt32 baseAddr, 
            UInt32 remoteId, UInt32 offEip, UInt32 offEsp)
        {
            if (_threadInfos.ContainsKey(tid))
            {
                throw new InvalidOperationException(
                    string.Format("Thread id={0} already exists", tid));
            }
            _threadInfos.Add(tid, new NativeThreadInfo(tid, handle, baseAddr, remoteId, offEip, offEsp));
            _eventQueue.Add(tid, new Queue<NativeEvent>());
            _tidMap.Add(remoteId, tid);
            _eventContinued[tid] = false;
        }

        internal void RemoveThread(uint id)
        {
            if (!_threadInfos.ContainsKey(id))
            {
                throw new InvalidOperationException(
                    string.Format("No thread with id {0}", id));
            }
            _threadInfos.Remove(id);
            _eventQueue.Remove(id);
            _eventContinued.Remove(id);
        }

        internal NativeModuleInfo GetNativeModuleInfo(UInt32 baseAddr)
        {
            if (!_moduleInfos.ContainsKey(baseAddr))
            {
                //throw new InvalidOperationException("No module with base " + baseAddr.ToString("x08"));
                return null;
            }
            return _moduleInfos[baseAddr];
        }

        internal CONTEXT GetThreadContext(IntPtr hThread, CONTEXT_FLAGS flags)
        {
            var context = new CONTEXT {ContextFlags = flags};
            if (!Win32Api.GetThreadContext(hThread, ref context))
            {
                throw new Exception();
            }
            return context;
        }

        internal void SetThreadContext(IntPtr hThread, ref CONTEXT context)
        {
            if (!Win32Api.SetThreadContext(hThread, ref context))
            {
                throw new Exception();
            }
        }

        internal void RunToAddress(uint tid, UInt32 addr)
        {
            var ctx = GetThreadContext(_threadInfos[tid].Handle, CONTEXT_FLAGS.CONTEXT_CONTROL);
            if (ctx.Eip == addr) return;

            byte origByte = SetInt3(addr);
            
            while (true)
            {
                var e = WaitForDebugEventThread(tid);
                if (!(e is ExceptionNativeEvent))
                    continue;

                var evt = e as ExceptionNativeEvent;
                if (evt.ExceptionCode != ExceptionCode.STATUS_BREAKPOINT)
                    continue;

                var hThread = _threadInfos[tid].Handle;
                var context = GetThreadContext(hThread, CONTEXT_FLAGS.CONTEXT_CONTROL);
                context.Eip--;
                if (context.Eip != addr)
                {
                    throw new Exception();
                }
                SetThreadContext(hThread, ref context);
                RestoreInt3(context.Eip, origByte);
                //SetTF(hThread);
                break;
            }
        }

        internal void SingleStep(uint tid)
        {
            while (true)
            {
                var e = WaitForDebugEventThread(tid);
                if (!(e is ExceptionNativeEvent)) continue;
                var evt = e as ExceptionNativeEvent;
                if (evt.ExceptionCode == ExceptionCode.STATUS_SINGLESTEP)
                {
                    break;
                }
            }
        }

        static readonly byte[] CC = new byte[]{ 0xcc };

        internal byte SetInt3(UInt32 addr)
        {
            var buf = new byte[1];
            int num;
            if (!Win32Api.ReadProcessMemory(Handle, addr, buf, (UIntPtr) 1, out num))
            {
                throw new InvalidOperationException("ReadProcessMemory");
            }
            if (!Win32Api.WriteProcessMemory(Handle, addr, CC, (UIntPtr) 1, out num))
            {
                throw new InvalidOperationException("WriteProcessMemory");
            }
            return buf[0];
        }

        internal byte RestoreInt3(UInt32 addr, byte orig)
        {
            var buf = new byte[] {orig};
            int num;
            if (!Win32Api.WriteProcessMemory(Handle, addr, buf, (UIntPtr) 1, out num))
            {
                throw new InvalidOperationException("WriteProcessMemory");
            }
            return buf[0];
        }

        internal void SetTF(IntPtr hThread)
        {
            var context = GetThreadContext(hThread, CONTEXT_FLAGS.CONTEXT_CONTROL);
            context.EFlags |= 0x100;
            SetThreadContext(hThread, ref context);
        }

        internal NativeEvent WaitForDebugEvent()
        {
            var evt = new DebugEvent();
            if (!Win32Api.WaitForDebugEvent(ref evt, Win32Api.INFINITE))
            {
                throw new InvalidOperationException("WaitForDebugEvent() failed with error code "
                    + Marshal.GetLastWin32Error());
            }
            _eventContinued[evt.Header.dwThreadId] = false;
            var e = NativeEvent.Build(this, ref evt.Header, ref evt.u);
            Console.WriteLine("- EVT : {0} -", e);
            return e;
        }

        internal void EnqueueDebugEvent(NativeEvent e)
        {
            _eventQueue[e.ThreadId].Enqueue(e);
        }

        internal NativeEvent WaitForDebugEventThread(uint tid)
        {
            var q = _eventQueue[tid];
            if (q.Count > 0)
                return q.Dequeue();
            while (true)
            {
                ContinueDebugEvent(tid);
                var e = WaitForDebugEvent();
                if (e.ThreadId == tid)
                    return e;
                EnqueueDebugEvent(e);
            }
        }

        internal NativeEvent WaitForDebugEventNew(uint tid)
        {
            while (true)
            {
                ContinueDebugEvent(tid);
                var e = WaitForDebugEvent();
                if (!HasThread(e.ThreadId))
                    return e;
                EnqueueDebugEvent(e);
                tid = e.ThreadId;
            }
        }

        internal void ContinueDebugEvent(NativeEvent nativeEvent)
        {
            if (nativeEvent == null)
            {
                throw new ArgumentNullException("nativeEvent");
            }
            Debug.Assert(nativeEvent.ProcessId == ProcessId);
            ContinueDebugEvent(nativeEvent.ThreadId);
        }

        internal void ContinueDebugEvent(uint tid)
        {
            // TODO
            if (_eventContinued[tid]) return;
            if (!Win32Api.ContinueDebugEvent(ProcessId, tid, Win32Api.ContinueStatus.DBG_CONTINUE))
            {
                throw new InvalidOperationException("ContinueDebugEvent() failed with error code "
                    + Marshal.GetLastWin32Error());
            }
            _eventContinued[tid] = true;
        }

        internal uint GetNativeThreadId(UInt32 tid)
        {
            return _tidMap[tid];
        }

        internal ContextData GetContextData(uint tid)
        {
            var tinfo = _threadInfos[tid];
            var context = GetThreadContext(tinfo.Handle, CONTEXT_FLAGS.CONTEXT_CONTROL);
            var data = new ContextData();
            data.Eip = context.Eip - tinfo.OffsetEip;
            data.Esp = context.Esp - tinfo.OffsetEsp;
            return data;
        }
    }
}
