﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32.SafeHandles;

namespace LochsEmuRemoteProc.Native
{
    [StructLayout(LayoutKind.Sequential)]
    class STARTUPINFO
    {
        public STARTUPINFO()
        {
            cb = Marshal.SizeOf(this);

            hStdInput = new SafeFileHandle(new IntPtr(0), false);
            hStdOutput = new SafeFileHandle(new IntPtr(0), false);
            hStdError = new SafeFileHandle(new IntPtr(0), false);
        }

        public Int32 cb;
        public string lpReserved;
        public string lpDesktop;
        public string lpTitle;
        public Int32 dwX;
        public Int32 dwY;
        public Int32 dwXSize;
        public Int32 dwYSize;
        public Int32 dwXCountChars;
        public Int32 dwYCountChars;
        public Int32 dwFillAttribute;
        public Int32 dwFlags;
        public Int16 wShowWindow;
        public Int16 cbReserved2;
        public IntPtr lpReserved2;
        public SafeFileHandle hStdInput;
        public SafeFileHandle hStdOutput;
        public SafeFileHandle hStdError;
    }

    [StructLayout(LayoutKind.Sequential)]
    class PROCESS_INFORMATION
    {
        public IntPtr hProcess;
        public IntPtr hThread;
        public uint dwProcessId;
        public uint dwThreadId;
    }

    //[StructLayout(LayoutKind.Sequential)]
    //public struct SECURITY_ATTRIBUTES
    //{
    //    public int nLength;
    //    public unsafe byte* lpSecurityDescriptor;
    //    public int bInheritHandle;
    //}

    enum DebugEventCode
    {
        None = 0,
        EXCEPTION_DEBUG_EVENT       = 1,
        CREATE_THREAD_DEBUG_EVENT   = 2,
        CREATE_PROCESS_DEBUG_EVENT  = 3,
        EXIT_THREAD_DEBUG_EVENT     = 4,
        EXIT_PROCESS_DEBUG_EVENT    = 5,
        LOAD_DLL_DEBUG_EVENT        = 6,
        UNLOAD_DLL_DEBUG_EVENT      = 7,
        OUTPUT_DEBUG_STRING_EVENT   = 8,
        RIP_EVENT                   = 9,
    }

    enum ThreadAccess : int
    {
        None = 0,
        THREAD_ALL_ACCESS = (0x1F03FF),
        THREAD_DIRECT_IMPERSONATION = (0x0200),
        THREAD_GET_CONTEXT = (0x0008),
        THREAD_IMPERSONATE = (0x0100),
        THREAD_QUERY_INFORMATION = (0x0040),
        THREAD_QUERY_LIMITED_INFORMATION = (0x0800),
        THREAD_SET_CONTEXT = (0x0010),
        THREAD_SET_INFORMATION = (0x0020),
        THREAD_SET_LIMITED_INFORMATION = (0x0400),
        THREAD_SET_THREAD_TOKEN = (0x0080),
        THREAD_SUSPEND_RESUME = (0x0002),
        THREAD_TERMINATE = (0x0001),
    }

    enum ExceptionCode : uint
    {
        None = 0x0, // included for completeness sake
        STATUS_BREAKPOINT = 0x80000003,
        STATUS_SINGLESTEP = 0x80000004,

        EXCEPTION_INT_DIVIDE_BY_ZERO = 0xC0000094,

        EXCEPTION_STACK_OVERFLOW = 0xC00000FD,
        EXCEPTION_NONCONTINUABLE_EXCEPTION = 0xC0000025,
        EXCEPTION_ACCESS_VIOLATION = 0xc0000005,
    }

    [Flags]
    enum ExceptionRecordFlags : uint
    {
        None = 0x0,
        EXCEPTION_NONCONTINUABLE = 0x1,
    }

    [StructLayout(LayoutKind.Sequential)]
    struct EXCEPTION_RECORD
    {
        public ExceptionCode ExceptionCode;
        public ExceptionRecordFlags ExceptionFlags;
        public IntPtr ExceptionRecord;
        public UInt32 ExceptionAddress;
        public UInt32 NumberParameters;
        //public const int EXCEPTOIN_MAXIMUM_PARAMETERS = 15;
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = EXCEPTOIN_MAXIMUM_PARAMETERS)]
        //public IntPtr[] ExceptionInformation;
        public IntPtr ExceptionInformation0;
        public IntPtr ExceptionInformation1;
        public IntPtr ExceptionInformation2;
        public IntPtr ExceptionInformation3;
        public IntPtr ExceptionInformation4;
        public IntPtr ExceptionInformation5;
        public IntPtr ExceptionInformation6;
        public IntPtr ExceptionInformation7;
        public IntPtr ExceptionInformation8;
        public IntPtr ExceptionInformation9;
        public IntPtr ExceptionInformation10;
        public IntPtr ExceptionInformation11;
        public IntPtr ExceptionInformation12;
        public IntPtr ExceptionInformation13;
        public IntPtr ExceptionInformation14;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct EXCEPTION_DEBUG_INFO
    {
        public EXCEPTION_RECORD ExceptionRecord;
        public UInt32 dwFirstChance;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct CREATE_PROCESS_DEBUG_INFO
    {
        public IntPtr hFile;
        public IntPtr hProcess;
        public IntPtr hThread;
        public UInt32 lpBaseOfImage;
        public UInt32 dwDebugInfoFileOffset;
        public UInt32 nDebugInfoSize;
        public IntPtr lpThreadLocalBase;
        public UInt32 lpStartAddress;
        public IntPtr lpImageName;
        public UInt16 fUnicode;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct CREATE_THREAD_DEBUG_INFO
    {
        public IntPtr hThread;
        public IntPtr lpThreadLocalBase;
        public UInt32 lpStartAddress;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct EXIT_THREAD_DEBUG_INFO
    {
        public UInt32 dwExitCode;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct EXIT_PROCESS_DEBUG_INFO
    {
        public UInt32 dwExitCode;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct LOAD_DLL_DEBUG_INFO
    {
        public IntPtr hFile;
        public UInt32 lpBaseOfDll;
        public UInt32 dwDebugInfoFileOffset;
        public UInt32 nDebugInfoSize;
        public IntPtr lpImageName;
        public UInt16 fUnicode;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct UNLOAD_DLL_DEBUG_INFO
    {
        public UInt32 lpBaseOfDll;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct OUTPUT_DEBUG_STRING_INFO
    {
        public IntPtr lpDebugStringData;
        public UInt16 fUnicode;
        public UInt16 nDebugStringLength;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct RIP_INFO
    {
        public uint dwError;
        public uint dwType;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct DebugEventHeader
    {
        public DebugEventCode dwDebugEventCode;
        public UInt32 dwProcessId;
        public UInt32 dwThreadId;
    };

    [StructLayout(LayoutKind.Explicit)]
    struct DebugEventUnion
    {
        [FieldOffset(0)]
        public CREATE_PROCESS_DEBUG_INFO CreateProcess;

        [FieldOffset(0)]
        public EXCEPTION_DEBUG_INFO Exception;

        [FieldOffset(0)]
        public CREATE_THREAD_DEBUG_INFO CreateThread;

        [FieldOffset(0)]
        public EXIT_THREAD_DEBUG_INFO ExitThread;

        [FieldOffset(0)]
        public EXIT_PROCESS_DEBUG_INFO ExitProcess;

        [FieldOffset(0)]
        public LOAD_DLL_DEBUG_INFO LoadDll;

        [FieldOffset(0)]
        public UNLOAD_DLL_DEBUG_INFO UnloadDll;

        [FieldOffset(0)]
        public OUTPUT_DEBUG_STRING_INFO OutputDebugString;

        [FieldOffset(0)] 
        public RIP_INFO RipInfo;
    }

    [StructLayout(LayoutKind.Explicit)]
    struct DebugEvent
    {
        [FieldOffset(0)] 
        public DebugEventHeader Header;

        [FieldOffset(12)] 
        public DebugEventUnion u;
    }

    // copy and paste boundary

    [StructLayout(LayoutKind.Sequential)]
    struct ModuleInfo
    {
        public IntPtr lpBaseOfDll;
        public uint SizeOfImage;
        public IntPtr EntryPoint;
    }

    static class Win32Api
    {
        public const uint INFINITE = 0xffffffff;
        public const uint MAX_PATH = 260;
        public const uint FILE_NAME_NORMALIZED = 0;

        private const string Kernel32 = "kernel32.dll";
        private const string Psapi = "psapi.dll";

        [DllImport(Kernel32, SetLastError = true, PreserveSig = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CloseHandle(IntPtr handle);

        [DllImport(Kernel32, SetLastError = true, PreserveSig = true)]
        public static extern int WaitForSingleObject(IntPtr hHandle, uint dwMilliseconds);

        [DllImport(Kernel32, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetThreadContext(IntPtr hThread, ref CONTEXT lpContext);

        [DllImport(Kernel32, SetLastError = true)]
        public static extern IntPtr OpenThread(ThreadAccess dwDesiredAccess,
            [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle,
            uint dwThreadId);

        [DllImport(Kernel32, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetThreadContext(IntPtr hThread, ref CONTEXT lpContext);

        [DllImport(Kernel32, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetFileSizeEx(IntPtr hFile, out Int64 lpFileSize);

        [DllImport(Psapi, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetModuleInformation(IntPtr hProcess, 
            IntPtr hModule, out ModuleInfo lpmodinfo, uint countBytes);

        [DllImport(Kernel32, SetLastError = true, PreserveSig = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ReadProcessMemory(IntPtr hProcess, UInt32 lpBaseAddress,
          byte[] lpBuffer, UIntPtr nSize, out int lpNumberOfBytesRead);

        [DllImport(Kernel32, SetLastError = true, PreserveSig = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool WriteProcessMemory(IntPtr hProcess, UInt32 lpBaseAddress,
          byte[] lpBuffer, UIntPtr nSize, out int lpNumberOfBytesWritten);

        [DllImport(Kernel32, SetLastError = true, PreserveSig = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DebugSetProcessKillOnExit(
            [MarshalAs(UnmanagedType.Bool)]
            bool KillOnExit
        );

        [DllImport(Kernel32, SetLastError = true, PreserveSig = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DebugBreakProcess(IntPtr hProcess);

        [Flags]
        public enum CreateProcessFlags
        {
            CREATE_NEW_CONSOLE = 0x00000010,
            DEBUG_PROCESS = 1,
            DEBUG_ONLY_THIS_PROCESS = 2,
        }

        [DllImport(Kernel32, CharSet = CharSet.Unicode, SetLastError = true, PreserveSig = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CreateProcess(
            string lpApplicationName,
            string lpCommandLine,
            IntPtr lpProcessAttributes,
            IntPtr lpThreadAttributes,
            [MarshalAs(UnmanagedType.Bool)]
            bool bInheritHandles,
            CreateProcessFlags dwCreationFlags,
            IntPtr lpEnvironment,
            string lpCurrentDirectory,
            STARTUPINFO lpStartupInfo,// class
            PROCESS_INFORMATION lpProcessInformation // class
        );

        [DllImport(Kernel32, SetLastError = true, PreserveSig = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DebugActiveProcess(uint dwProcessId);

        [DllImport(Kernel32, SetLastError = true, PreserveSig = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DebugActiveProcessStop(uint dwProcessId);

        [DllImport(Kernel32, SetLastError = true, PreserveSig = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool TerminateProcess(IntPtr hProcess, 
            uint uExitCode);

        [DllImport(Kernel32, SetLastError = true, PreserveSig = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool WaitForDebugEvent(ref DebugEvent pDebugEvent, 
            uint dwMilliseconds);

        public enum ContinueStatus : uint
        {
            CONTINUED = 0,
            DBG_CONTINUE = 0x00010002,
            DBG_EXCEPTION_NOT_HANDLED = 0x80010001,
        }

        [DllImport(Kernel32, SetLastError = true, PreserveSig = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ContinueDebugEvent(uint dwProcessId, 
            uint dwThreadId, ContinueStatus dwContinueStatus);

        [DllImport(Kernel32, SetLastError = true)]
        public static extern uint GetFinalPathNameByHandle(IntPtr hFile, 
            StringBuilder lpszFilePath, uint cchFilePath, uint dwFlags);
    }

    [Flags]
    public enum CONTEXT_FLAGS : uint
    {
        CONTEXT_i386 = 0x10000,
        CONTEXT_i486 = 0x10000,   //  same as i386
        CONTEXT_CONTROL = CONTEXT_i386 | 0x01, // SS:SP, CS:IP, FLAGS, BP
        CONTEXT_INTEGER = CONTEXT_i386 | 0x02, // AX, BX, CX, DX, SI, DI
        CONTEXT_SEGMENTS = CONTEXT_i386 | 0x04, // DS, ES, FS, GS
        CONTEXT_FLOATING_POINT = CONTEXT_i386 | 0x08, // 387 state
        CONTEXT_DEBUG_REGISTERS = CONTEXT_i386 | 0x10, // DB 0-3,6,7
        CONTEXT_EXTENDED_REGISTERS = CONTEXT_i386 | 0x20, // cpu specific extensions
        CONTEXT_FULL = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_SEGMENTS,
        CONTEXT_ALL = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_SEGMENTS | CONTEXT_FLOATING_POINT | CONTEXT_DEBUG_REGISTERS | CONTEXT_EXTENDED_REGISTERS
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct FLOATING_SAVE_AREA
    {
        public uint ControlWord;
        public uint StatusWord;
        public uint TagWord;
        public uint ErrorOffset;
        public uint ErrorSelector;
        public uint DataOffset;
        public uint DataSelector;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
        public byte[] RegisterArea;
        public uint Cr0NpxState;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CONTEXT
    {
        public CONTEXT_FLAGS ContextFlags; //set this to an appropriate value
        // Retrieved by CONTEXT_DEBUG_REGISTERS
        public uint Dr0;
        public uint Dr1;
        public uint Dr2;
        public uint Dr3;
        public uint Dr6;
        public uint Dr7;
        // Retrieved by CONTEXT_FLOATING_POINT
        public FLOATING_SAVE_AREA FloatSave;
        // Retrieved by CONTEXT_SEGMENTS
        public uint SegGs;
        public uint SegFs;
        public uint SegEs;
        public uint SegDs;
        // Retrieved by CONTEXT_INTEGER
        public uint Edi;
        public uint Esi;
        public uint Ebx;
        public uint Edx;
        public uint Ecx;
        public uint Eax;
        // Retrieved by CONTEXT_CONTROL
        public uint Ebp;
        public uint Eip;
        public uint SegCs;
        public uint EFlags;
        public uint Esp;
        public uint SegSs;
        // Retrieved by CONTEXT_EXTENDED_REGISTERS
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
        public byte[] ExtendedRegisters;

    } 
}
