﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LochsEmuRemoteProc.Native
{
    class NativeEvent
    {
        protected DebugEventHeader _header;
        protected DebugEventUnion _union;
        protected NativeProcess _process;


        protected NativeEvent(NativeProcess process, 
            ref DebugEventHeader header, ref DebugEventUnion union)
        {
            _process = process;
            _header = header;
            _union = union;
        }

        static internal NativeEvent Build(NativeProcess process, 
            ref DebugEventHeader header, ref DebugEventUnion union)
        {
            switch (header.dwDebugEventCode)
            {
                case DebugEventCode.CREATE_PROCESS_DEBUG_EVENT:
                    return new CreateProcessDebugEvent(process, ref header, ref union);
                case DebugEventCode.CREATE_THREAD_DEBUG_EVENT:
                    return new CreateThreadNativeEvent(process, ref header, ref union);
                case DebugEventCode.EXCEPTION_DEBUG_EVENT:
                    return new ExceptionNativeEvent(process, ref header, ref union);
                case DebugEventCode.EXIT_PROCESS_DEBUG_EVENT:
                    return new ExitProcessDebugEvent(process, ref header, ref union);
                case DebugEventCode.EXIT_THREAD_DEBUG_EVENT:
                    return new ExitThreadNativeEvent(process, ref header, ref union);
                case DebugEventCode.LOAD_DLL_DEBUG_EVENT:
                    return new LoadDllNativeEvent(process, ref header, ref union);
                case DebugEventCode.OUTPUT_DEBUG_STRING_EVENT:
                    return new OutputDebugStringNativeEvent(process, ref header, ref union);
                case DebugEventCode.UNLOAD_DLL_DEBUG_EVENT:
                    return new UnloadDllNativeEvent(process, ref header, ref union);
                case DebugEventCode.RIP_EVENT:
                    throw new NotImplementedException();
                default:
                    throw new Exception("wtf, code = " + header.dwDebugEventCode);
            }
        }

        public NativeProcess Process
        {
            get { return _process; }
        }

        public DebugEventCode EventCode
        {
            get { return _header.dwDebugEventCode; }
        }

        public uint ThreadId
        {
            get { return _header.dwThreadId; }
        }

        public uint ProcessId
        {
            get { return _header.dwProcessId; }
        }
    }

    class CreateProcessDebugEvent : NativeEvent
    {
        internal CreateProcessDebugEvent(NativeProcess process, 
            ref DebugEventHeader header, ref DebugEventUnion union)
            : base(process, ref header, ref union)
        {
            Process.Init(ref union.CreateProcess);
        }

        public override string ToString()
        {
            return string.Format("CreateProcess Pid={2} Tid={3} [{0}] at base {1:x08}", 
                Process.Path, Process.BaseOfImage, ProcessId, ThreadId);
        }
    }

    class ExitProcessDebugEvent : NativeEvent
    {
        internal ExitProcessDebugEvent(NativeProcess process, 
            ref DebugEventHeader header, ref DebugEventUnion union)
            : base(process, ref header, ref union)
        {
            Process.ClearHandle();
        }

        public override string ToString()
        {
            return string.Format("ExitProcess Pid={1} Tid={2}, code={0}",
                                 _union.ExitProcess.dwExitCode, ProcessId, ThreadId);
        }
    }

    class OutputDebugStringNativeEvent : NativeEvent
    {
        internal OutputDebugStringNativeEvent(NativeProcess process, 
            ref DebugEventHeader header, ref DebugEventUnion union)
            : base(process, ref header, ref union)
        {
            throw new NotImplementedException();
        }
    }

    abstract class DllNativeEvent : NativeEvent
    {
        internal DllNativeEvent(NativeProcess process, 
            ref DebugEventHeader header, ref DebugEventUnion union)
            : base(process, ref header, ref union)
        {
            
        }
    }

    class LoadDllNativeEvent : NativeEvent
    {
        internal LoadDllNativeEvent(NativeProcess process, 
            ref DebugEventHeader header, ref DebugEventUnion union)
            : base(process, ref header, ref union)
        {
            _imagePath = Utils.GetPathByHandle(union.LoadDll.hFile).Substring(4);
            Process.AddModule(union.LoadDll.lpBaseOfDll, _imagePath);
        }

        private readonly string _imagePath;

        public override string ToString()
        {
            return string.Format("LoadDll [{0}] at base {1:x08}",
                                 _imagePath, _union.LoadDll.lpBaseOfDll);
        }
    }

    class UnloadDllNativeEvent : NativeEvent
    {
        internal UnloadDllNativeEvent(NativeProcess process, 
            ref DebugEventHeader header, ref DebugEventUnion union)
            : base(process, ref header, ref union)
        {
            _info = Process.GetNativeModuleInfo(union.UnloadDll.lpBaseOfDll);
        }

        private readonly NativeModuleInfo _info;

        public override string ToString()
        {
            return string.Format("UnloadDll [{0}] at base {1:x08}",
                                 _info == null ? "Unknown" : _info.ImagePath,
                                 _union.UnloadDll.lpBaseOfDll);
        }
    }

    class CreateThreadNativeEvent : NativeEvent
    {
        internal CreateThreadNativeEvent(NativeProcess process, 
            ref DebugEventHeader header, ref DebugEventUnion union)
            : base(process, ref header, ref union)
        {
        }

        public IntPtr Handle
        {
            get { return _union.CreateThread.hThread; }
        }

        public UInt32 StartAddress
        {
            get { return _union.CreateThread.lpStartAddress; }
        }

        public override string ToString()
        {
            return string.Format("CreateThread Tid={1}, base={0:x08}", 
                                 _union.CreateThread.lpStartAddress,
                                 _header.dwThreadId);
        }
    }

    class ExitThreadNativeEvent : NativeEvent
    {
        internal ExitThreadNativeEvent(NativeProcess process, 
            ref DebugEventHeader header, ref DebugEventUnion union)
            : base(process, ref header, ref union)
        {
            
        }

        public override string ToString()
        {
            return string.Format("ExitThread Tid={1}, code={0}", 
                                 _union.ExitProcess.dwExitCode,
                                 _header.dwThreadId);
        }
    }

    class ExceptionNativeEvent : NativeEvent
    {
        internal ExceptionNativeEvent(NativeProcess process, 
            ref DebugEventHeader header, ref DebugEventUnion union)
            : base(process, ref header, ref union)
        {
            // TODO
        }

        public ExceptionCode ExceptionCode
        {
            get { return _union.Exception.ExceptionRecord.ExceptionCode; }
        }

        public UInt32 Address
        {
            get { return _union.Exception.ExceptionRecord.ExceptionAddress; }
        }

        public override string ToString()
        {
            return string.Format("Exception Tid={0}, code={1}, address={2:x08}",
                                 _header.dwThreadId, ExceptionCode, Address);
        }
    }
}
