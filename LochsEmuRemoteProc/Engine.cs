﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using LochsEmuRemoteProc.Comm;
using LochsEmuRemoteProc.Native;

namespace LochsEmuRemoteProc
{
    public class Engine
    {
        private readonly PipeClient _pipe = new PipeClient();
        private const string PipeName = "lochsemu";
        private NativeProcess _process;

        public void Run(string servername)
        {
            _pipe.Connect(servername, PipeName);

            // TODO : Read path and command lines from remote

            const string DebugAppPath = @"e:\Code\LochsEmuSamples\TaintThread\Release\TaintThread.exe";
            _process = CreateProcessDebug(DebugAppPath, null);


            var e = _process.WaitForDebugEvent();
            if (!(e is CreateProcessDebugEvent))
            {
                throw new Exception("should be CreateProcessDebugEvent");
            }
            OnCreateProcess(e as CreateProcessDebugEvent);
            _process.ContinueDebugEvent(e);

            while (true)
            {
                e = _process.WaitForDebugEvent();
                if (e is ExceptionNativeEvent)
                {
                    OnSystemBreakpoint(e as ExceptionNativeEvent);
                    break;
                }
                _process.ContinueDebugEvent(e);
            }

            Loop();
        }

        private void Loop()
        {
            while (true)
            {
                var data = _pipe.Read<SyncEventData>();
                switch (data.EventType)
                {
                    case SyncEvent.SingleStep:
                        OnSingleStep(data.SingleStep);
                        break;
                    case SyncEvent.ThreadCreate:
                        OnThreadCreate(data.ThreadCreate);
                        break;
                    case SyncEvent.ProcessExit:
                        throw new NotImplementedException();
                    case SyncEvent.ThreadExit:
                        throw new NotImplementedException();
                    default:
                        throw new Exception("EventType = " + data.EventType);
                }
            }
        }

        internal NativeProcess CreateProcessDebug(string path, string commandArgs)
        {
            if (_process != null)
            {
                throw new InvalidOperationException("Debug process is already created");
            }

            var pi = new PROCESS_INFORMATION();
            var si = new STARTUPINFO();

            if (!Win32Api.CreateProcess(
                path,
                commandArgs,
                IntPtr.Zero,
                IntPtr.Zero,
                false,
                //Win32Api.CreateProcessFlags.CREATE_NEW_CONSOLE |
                Win32Api.CreateProcessFlags.DEBUG_ONLY_THIS_PROCESS,
                IntPtr.Zero,
                null,
                si,
                pi))
            {
                throw new InvalidOperationException("CreateProcess() failed with error code "
                    + Marshal.GetLastWin32Error());
            }

            Win32Api.CloseHandle(pi.hProcess);
            Win32Api.CloseHandle(pi.hThread);

            _process = new NativeProcess(pi.dwProcessId, pi.dwThreadId);
            return _process;
        }

        private void OnCreateProcess(CreateProcessDebugEvent e)
        {
            var data = _pipe.Read<SyncEventData>();
            if (data.EventType != SyncEvent.ProcessCreate)
            {
                throw new Exception();
            }

            var pc = data.ProcessCreate;

            Console.WriteLine("LochsEmu process created, base={0:x08}, entry={1:x08}",
                pc.ImageBase, pc.StartAddress);

            _process.CalculateOffset(pc.ImageBase, pc.StartAddress);
            _process.AddThread(_process.ThreadId, _process.HandleThread, 
                _process.StartAddress, pc.ThreadId, _process.Offset, pc.Esp);   
            // HACK: first store Esp in offsetEsp, then later fix that
        }

        private void OnSystemBreakpoint(ExceptionNativeEvent e)
        {
            Console.WriteLine("System breakpoint at {0:x08}", e.Address);

            Console.WriteLine("Running to entry {0:x08}", _process.StartAddress);
            _process.RunToAddress(_process.ThreadId, _process.StartAddress);

            var context = _process.GetThreadContext(_process.HandleThread, CONTEXT_FLAGS.CONTEXT_CONTROL);
            var ti = _process.GetThreadInfo(_process.ThreadId);
            UInt32 offEsp = unchecked(context.Esp - ti.OffsetEsp);
            ti.OffsetEsp = offEsp;
            //_process.GetThreadInfo(_process.ThreadId).OffsetEsp = offEsp;
        }

        private void OnSingleStep(SingleStepData d)
        {
            Console.WriteLine("SingleStep at {0:x08}, Tid = {2}, multiInsts = {1}", 
                d.Eip, d.MultiInsts, d.ThreadId);
            uint tid = _process.GetNativeThreadId(d.ThreadId);
            //if (d.MultiInsts)
            //{
                _process.RunToAddress(tid, d.Eip);
            //}
            //else
            //{
                //_process.SingleStep(tid);
            //}
            //_process.SetTF(_process.GetThreadInfo(tid).Handle);

            var data = _process.GetContextData(tid);
            Console.WriteLine("Sending eip={0:x08} esp={1:x08}", data.Eip, data.Esp);
            _pipe.Write(new SyncEventData {EventType = SyncEvent.Context, Context = data});
        }

        private void OnThreadCreate(ThreadCreateData d)
        {
            var parentId = _process.GetNativeThreadId(d.ParentTid);
            //while (true)
            //{
                //_process.ContinueDebugEvent(parentId);
                var e = _process.WaitForDebugEventNew(parentId);
                //if (_process.HasThread(e.ThreadId))
                //{
                //    _process.EnqueueDebugEvent(e);
                //    continue;
                //}

                if (!(e is CreateThreadNativeEvent))
                {
                    throw new Exception("wtf");
                }
                var evt = e as CreateThreadNativeEvent;


                _process.AddThread(evt.ThreadId, evt.Handle, evt.StartAddress,
                    d.Tid, 0, 0);

                _process.RunToAddress(evt.ThreadId, evt.StartAddress);
                var context = _process.GetThreadContext(evt.Handle, CONTEXT_FLAGS.CONTEXT_CONTROL);
                UInt32 offEip = context.Eip - d.StartAddress;
                UInt32 offEsp = context.Esp - d.Esp;

                _process.GetThreadInfo(evt.ThreadId).OffsetEip = offEip;
                _process.GetThreadInfo(evt.ThreadId).OffsetEsp = offEsp;
                //break;
            //}
        }
    }
}
