﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LochsEmuRemoteProc.Comm
{
    enum SyncEvent
    {
        Invalid = 0,
        SingleStep,
        Context,
        ProcessCreate,
        ProcessExit,
        ThreadCreate,
        ThreadExit,
    }

    [StructLayout(LayoutKind.Sequential)]
    struct SingleStepData
    {
        public UInt32 ThreadId;
        public UInt32 Eip;
        public bool MultiInsts;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct ContextData
    {
        public UInt32 Eip;
        public UInt32 Esp;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct ProcessCreateData
    {
        public UInt32 ImageBase;
        public UInt32 StartAddress;
        public UInt32 ThreadId;
        public UInt32 Esp;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct ThreadCreateData
    {
        public UInt32 ParentTid;
        public UInt32 Tid;
        public UInt32 StartAddress;
        public UInt32 Esp;
    }

    [StructLayout(LayoutKind.Explicit)]
    struct SyncEventData
    {
        [FieldOffset(0)]
        public SyncEvent EventType;

        [FieldOffset(4)] 
        public SingleStepData SingleStep;

        [FieldOffset(4)] 
        public ContextData Context;

        [FieldOffset(4)] 
        public ProcessCreateData ProcessCreate;

        [FieldOffset(4)] 
        public ThreadCreateData ThreadCreate;
    }
}
