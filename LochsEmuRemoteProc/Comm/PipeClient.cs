﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.IO.Pipes;

namespace LochsEmuRemoteProc.Comm
{
    internal class PipeClient : IDisposable
    {
        private NamedPipeClientStream _pipe;
        private const int BufferSize = 256;

        public void Connect(string servername, string pipename)
        {
            const string HelloString = "hello";

            Console.WriteLine("Trying to connect to \\\\{0}\\pipe\\{1}",
                servername, pipename);

            _pipe = new NamedPipeClientStream(servername, pipename,
                                                       PipeDirection.InOut, 
                                                       PipeOptions.None);
            _pipe.Connect();

            var t = ReadString();
            if (t != HelloString)
            {
                throw new Exception("Incorrect hello string: " + t);
            }

            Console.WriteLine("Server hello");

            WriteString(HelloString);

            Console.WriteLine("Pipe connection established");
        }

        public string ReadString()
        {
            if (_pipe == null)
                throw new InvalidOperationException("not connected");
            var data = new byte[BufferSize];
            _pipe.Read(data, 0, BufferSize);
            var zeroIndex = Array.IndexOf(data, (byte) 0);
            if (zeroIndex == -1) zeroIndex = BufferSize;
            return Encoding.ASCII.GetString(data, 0, zeroIndex);
        }

        public void WriteString(string s)
        {
            var data = Encoding.Default.GetBytes(s);
            if (data.Length > BufferSize) 
                throw new Exception();
            _pipe.Write(data, 0, data.Length);
        }

        public T Read<T>() where T : struct
        {
            var size = Marshal.SizeOf(typeof (T));
            var data = new byte[size];
            _pipe.Read(data, 0, size);

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(data, 0, ptr, size);
            var t = (T) Marshal.PtrToStructure(ptr, typeof (T));
            Marshal.FreeHGlobal(ptr);
            return t;
        }

        public void Write<T>(T t) where T : struct
        {
            var size = Marshal.SizeOf(t);
            var data = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(t, ptr, true);
            Marshal.Copy(ptr, data, 0, size);
            Marshal.FreeHGlobal(ptr);

            _pipe.Write(data, 0, size);
        }

        public void Dispose()
        {
            if (_pipe != null)
                _pipe.Dispose();
        }
    }
}
